/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false */
/* jshint strict: false, -W117 */
angular.module('starter.controllers', ['ionic', 'ngCordova'])

.controller('DashCtrl',function($scope, $ionicActionSheet, $timeout, $rootScope, $ionicBackdrop, $cordovaDevice, $cordovaCamera, $http, $interval) {




  var canvas = document.getElementById('circle');
  var context = canvas.getContext('2d');

  var centerX = canvas.width / 2;
  var centerY = canvas.height / 2;
  var radius = 70;

  context.beginPath();
  context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
  context.fillStyle = 'green';
  context.fill();
  context.lineWidth = 5;
  context.strokeStyle = '#003300';
  context.stroke();



  /*context.beginPath();
  context.moveTo(20, 20);
  context.quadraticCurveTo(20,100,200,20);
  context.stroke();*/

   $scope.clickUUID = function () {

     $scope.uuiddee  = $cordovaDevice.getUUID();
     /*$scope.uuiddee = $scope.logtext +
       "设备：" + device.cordova + "\n" +
       "cordova版本：" + cordova + "\n" +
       "设备版本：" + model + "\n" +
       "运行平台：" + platform + "\n" +
       "唯一标识符：" + uuid + "\n" +
       "版本：" + version + "\n" ;*/
   };
  $scope.onswipteleft = function () {
      $scope.success = "left";
  };

  $scope.takeddd = function () {

    var options = {
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
    };

    $cordovaCamera.getPicture(options).then(function(imageURI) {
      var image = document.getElementById('myImage');
      image.src = imageURI;
    }, function(err) {
      // error
    });
  };

  $scope.plusplus = function () {
    cordova.plugins.TheMath.plusaaa([2, 5], function (msg) {
      $scope.success = msg + "";
    }, function (msg) {

    });
  };
  $scope.scanthebar = function () {
    cordova.plugins.barcodeScanner.scan(
      function (result) {
        alert("We got a barcode\n" +
          "Result: " + result.text + "\n" +
          "Format: " + result.format + "\n" +
          "Cancelled: " + result.cancelled);
      },
      function (error) {
        alert("Scanning failed: " + error);
      }
    );
  };

  $scope.permissions = function () {
    var permissions = cordova.plugins.permissions;
    permissions.hasPermission(permissions.ACCESS_COARSE_LOCATION, function (status) {
      if ( status.hasPermission ) {
        console.log("Yes :D ");
      }
      else {
        console.warn("No :( ");
        permissions.requestPermission(permissions.CAMERA, function (status) {
          if( !status.hasPermission ) {
            console.warn('error');
          }
        }, function () {
          console.warn('Camera permission is not turned on');
        });
      }
    });
  };


  $scope.httpba = function () {
    $http.get('https://www.runoob.com/try/angularjs/data/sites.php').success(function (data) {

      $scope.httpresult = outputObj(data);
      var fromJson = angular.fromJson(data);
      $scope.httpresult2 = fromJson;
    });
  };
  var i = 1;
  $scope.intervalhaoba = function () {
    $interval(function () {
      $scope.httpresult = i++ + "秒";
    }, 1000);
  };


  function outputObj(obj) {
      var description = "";
      for (var i in obj) {
        description += i + " = " + obj[i] + "\n";
      }
    return description;
  };

  // Execute action on backdrop disappearing
  $scope.$on('backdrop.hidden', function() {

    alert("hidden");
  });

  // Execute action on backdrop appearing
  $scope.$on('backdrop.shown', function() {
    alert("show");
  });
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, $rootScope) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('TheCanvasCtrl', function ($scope) {
  //获取屏幕尺寸
  var winW;
  var winH;
  winW = window.screen.width;
  winH = window.screen.height;
  var canvas = document.getElementById("mycanvas");
  canvas.width = winW;
  canvas.height = winH;
  var cxt = canvas.getContext("2d");
  cxt.fillStyle = "black";
  cxt.fillRect(0,0,winW,winH);

  //确定屏幕出现字幕的栏数
  var str = "为之则易不为则难！！！";
  var fontSize = 20;
  var clos = Math.floor(winW/fontSize);

  //初始化纵坐标
  var array = new Array();
  for (var i=0;i<clos;i++){
    array.push(Math.random()*winH);
  }
  //画出字幕
  function drawStr(){
    cxt.fillStyle = "rgba(0,0,0,0.3)";
    cxt.fillRect(0,0,winW,winH);
    cxt.font = "700 "+fontSize+"px 微软雅黑";
    cxt.fillStyle = randomColor();
    for (var i = 0; i < clos; i++){
      var x = fontSize*i;
      var y = array[i]*fontSize;
      var indexStr = Math.floor(Math.random()*str.length);

      cxt.fillText(str[indexStr],x,y);
      if(y>canvas.height && Math.random()>0.96){
        array[i] = 0;
      }
      array[i]++;
    }
  }
  //随机生成字体颜色
  function randomColor(){
    var R = Math.floor(Math.random()*256);
    var G = Math.floor(Math.random()*256);
    var B = Math.floor(Math.random()*256);
    return "rgb"+"("+R+","+G+","+B+")";
  }

  setInterval(drawStr,33);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
